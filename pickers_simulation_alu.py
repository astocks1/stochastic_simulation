import numpy as np
import matplotlib.pyplot as plt
from PickersData import PickersData
import random
import statistics
import csv


##################### Funciones dadas ######################
def get_pdf(data):
	''' Dado un vector con muestras de una variable, retorna dos listas con los valores (a intervalos de 1) y su correspondiente frecuencia de ocurrencia'''
	bins = range(1, int(max(data)+1))
	count, _ = np.histogram(data, bins=bins)
	probs = count / sum(count)
	return bins,probs

def get_cdf(probs):
	''' Dado un vector con probabilidades (pdf), retorna un vector de la misma dimension con la cdf. Probs puede ser la lista probs que retorna
	get_pdf.'''
	return np.cumsum(probs)

def get_picker(pickers_stat):
	''' Dada una lista con el tiempo en que cada picker termina su orden actual, devuelve el indice (i.e., picker) que termina primero'''
	return pickers_stat.index(min(pickers_stat))

def analyze_results(total_times,total_revs,n_pickers, outcome):
	''' Fucion que concentra el analisis de los resultados para un determinado escenario y con una cantidad de pickers dada.
	@param total_times: lista (de float) que en la posicion i tiene tiempo total de ejecutar la i-esima simulacion.
	@param total_revs: lista (de float) que en la posicion i tiene el revenue total reolectado para la i-esima simulacion.'''

	print('Outcome: ', outcome)
	print('Cantidad Pickers' , n_pickers)
	print('Completados en 60 mins:',get_prob_completed(total_times,3600))
	print('Tiempo promedio:',statistics.mean(total_times))
	print('Rev. promedio:',statistics.mean(total_revs))

	# Convertimos a minutos para agrupar.
	total_times_mins = [int(i/60.0) for i in total_times]
	b = range(max(total_times_mins) + 1)

	# Cuando querramos graficar, descomentamos.
	plt.hist(total_times_mins, bins = b, density=True)
	plt.show()

	plt.hist(total_times_mins, bins = b, density=True, cumulative=True)
	plt.show()


def get_prob_completed(times, value):
	success = [t for t in times if t <= value]
	return len(success)/len(times)
############################################################

def simulate_uniform(a,b):
	''' Simula una Unif(a,b). Debe convertir el resultado a un int'''
	# Borrar pass y reemplazar por el codigo.
	## Devuelve valor entero de uniforme entre los parametros a y b
	prob = a + (b-a)*random.random()
	return int(prob)


def simulate_empirical(vals, cumprobs):
	''' Genera un numero aleatorio en base a una distribucion empirica usando el metodo de la transformada inversa.
	@param vals: posibles valores de la distribucion
	@param cumprobs: probabilidades acumuladas (cdf) para cada valor en vals.
	@return valor v (perteneciente a vals) generado aleatoriamente'''
	# Borrar pass e implementar lo que sea necesario
	## Generar numero aleatorio para encontrar por método de la acumulada inversa y almacena en rand.
	rand = random.random()
	## Busca el index del numero mas cercano a rand y almacena en pos.
	pos = min(range(len(cumprobs)), key=lambda i: abs(cumprobs[i]-rand))
	## retorna el item en vals que encontró anteriormente
	return vals[pos]

def generate_orders(n_orders, items_vals, items_cumprobs):
	''' Simula la cantidad de itmes que tendra cada orden. La idea es usar la funcion simulate_empirical. Para eso, recibe:
	@param n_orders: cuantas ordenes deben ser generadas.
	@param items_vals: posibles valores para la cantidad de items de una orden.
	@param items_cumprobs: probabilidades acumuladas para cada valor en items_vals.
	@return lista de n_orders elementos, donde cada posicion tiene un int generado aleatoriamente siguiendo la distribucion (items_vals, items_probs)'''
	# Borrar pass e implementar lo que sea necesario
	## agrega a lista res la cantidad de items simulados para los n_orders
	res = []
	for n in range(n_orders):
		sim = simulate_empirical(items_vals, items_cumprobs)
		res.append(sim)
	return res


def simulate_order_time(n_items, picker, pickers_dist):
	''' Simulamos el tiempo que le lleva a un picker procesar una determinada orden. Recordar que una orden esta definida por el numero de items
	que tiene, y que cada picker tiene su propia distribucion de tiempos de inter-picks.
	@param n_times: cantidad de items del picker.
	@param picker: id (0 a 9) del picker que atiende la orden.
	@param pickers_dist: diccionario con la info de todos los pickers.
	@return float con el tiempo total de procesar la orden, que consiste en la suma de los tiempos inter-picks entre ordenes.
	Ayuda: en pickers_dist, para acceder a los tiempos y probs. acumuladas del picker se puede hacer:
		pickers_dist[picker]['times']
		pickers_dist[picker]['cumprobs']
	'''
	# Borrar pass e implementar lo que sea necesario
	## Generar numero aleatorio para encontrar por método de la acumulada inversa y almacena en rand.
	rand = random.random()
	## para cada item de la orden, generar simular cuanto lleva buscarlo y sumarlo a la orden
	tiempo_acumulado = 0.0
	for n in range(n_items):
		sim = simulate_empirical(pickers_dist[picker]['times'], pickers_dist[picker]['cumprobs'])
		tiempo_acumulado += sim
	return tiempo_acumulado

def process_orders(orders, pickers_dist, n_pickers):
	''' Procesamos una serie de ordenes correspondiente a una simulacion. Recibe:
	@param orders: una lista con la cantidad de items de cada orden (como devuelve la funcion generate_orders).
	@param pickers_dist: diccionario definido con key = id de picker (0 a 9), y value un diccionario con los tiempos posibles y su correspondiente cdf.
	@param n_pickers: indica que se utilizan solo los primeros pickers.
	@return dos valores: el tiempo total en completar todas las ordenes, y el revenue generado.'''

	# Estado de los pickers. Inicialmente todos en el inicio del horizonte.
	pickers_stat = [0.0]*n_pickers
	# Revenue recolectado durante la simulacion.
	collected_rev = 0.0

	# Procesamos las ordenes una por una, en el orden que fueron generadas.
	for order in orders:
		# Buscamos primer picker en liberarse.
		available_picker = pickers_stat.index(min(pickers_stat))
		# Dado el picker, simulamos su tiempo.
		time = simulate_order_time(order, available_picker, pickers_dist)
		# Actualizamos el tiempo del picker.
		pickers_stat[available_picker] = pickers_stat[available_picker] + time
		# Verificamos si entro o no dentro de los 3600 segundos (1 hora) para recolectar el revenue.
		if time < 3600:
			collected_rev += 100
		# Devolvemos tiempo y revenue.
		last_to_finish = max(pickers_stat)
	return last_to_finish, collected_rev


def simulate_process(pickers_data, outcome, number_pickers, n_sims):
	''' Dado un posible outcome (bajo, moderado, alto) que tendra los limites esperados para la Unif(a,b) que modela la cantidad de ordenes,
	queremos realizar n_sims simulaciones y analizar el tiempo requerido para completarlas y el revenue generado.
	@param pickers_data: la estructura con los datos de la instancia, y las distribuciones para num. de items y pickers.
	@param outcome: una tupla de dos posiciones con los valores a,b de la variable Unif(a,b) que representa el escenario a analizar.
	@param number_pickers: el numero de pickers que se deben utilizar en la simulacion. No necesariamente es el maximo disponible.
	@param n_sim: cantidad de simulaciones a realizar.
	@return tupla con dos listas, una con los tiempos totales y otra con los revenues totales de cada simulacion.'''

	# Distribucion del numero de items. Reemplazar None.
	# Notar que las funciones para obtener las frecuencias y las acumuladas estan dadas al principio del archivo.

	##Crea lista vacía y toma data de pickers_data
	items = []
	for item in pickers_data.items_per_order:
		items.append(item)

	## Pasa data tomada de pickers_data y la pasa a pdf y cdf
	items_vals, items_probs = get_pdf(items)
	items_cumprobs = get_cdf(items_probs)

	# Distribucion de tiempos de viaje de cada picker.
	# Es un dict que tiene:
	# 	-clave: id de picker (0 a 9)
	# 	-value: otro diccionario, con dos posibles claves:
	#		- 'times', donde el valor seran los posibles valores de la distribucion de tiempos inter-picks.
	#		- 'cumprobs', que tendra la freq. acumulada para cada uno de los valores en 'times'.
	pickers_dist = {}
	##Inserto data de pickers_data.picker_times[picker] a diccionario
	for picker in pickers_data.picker_times:
		# Reemplazar los None
		times, probs = get_pdf(pickers_data.picker_times[picker])
		cumprobs = get_cdf(probs)
		pickers_dist[picker] = {'times': times, 'cumprobs': cumprobs}


	# Guardamos los resultados de tiempo y revenue para cada simulacion dado un posible outcome y un numero de pickers.
	total_times = []
	total_revs = []

	# Simular.
	for i in range(n_sims):
		# Simulamos primero el numero de ordenes. Recordar que para la Unif(a,b), a = outcome[0] y b = outcome[1].
		n_orders = simulate_uniform(outcome[0], outcome[1])
		# Generamos las n_orders, cada una con su cantidad de items, usando la funcion generate_orders.
		orders = generate_orders(n_orders, items_vals, items_cumprobs)
		# Procesamos las ordenes generadas con la funcion process_orders.
		time, rev = process_orders(orders, pickers_dist, number_pickers)
		# Recordar que process_orders devuelve dos valores: (i) el tiempo total de la simulacion actual, (ii) el revenue recolectado
		total_times.append(time)
		total_revs.append(rev)
	return total_times,total_revs

def main():

	# Fijar la semilla para debug.
	random.seed(0)

	# Data base
	max_number_pickers = 10 # No tocar este valor.
	number_of_simulations = 1000
	# Definicion de los posibles escenarios como una lista de tuplas, donde cada tupla representa los valores Unif(a,b)
	# Impacto bajo: Unif(20,25), posicion 0 de la lista scenarios.
	# Impacto moderado: Unif(30,40), posicion 1 de la lista scenarios.
	# Impacto alto: Unif(45,60), posicion 2 de la lista scenarios.
	scenarios = [(20,25),(30,40),(45,60)]

	# Se leen los datos de archivo y se procesan. No deberian modificar esto. Es algo simple de hacer (pero molesto!)
	data = PickersData(max_number_pickers)

	####### Van dos posibles formas de resolverlos, segun cuan comomdos se sientan con el codigo.
	### Posibilidad 1: resolver el ejercicio de manera automática pasando por todas las combinaciones de outcome,n_pickers.
	# Simular
	# Movemos los escenarios.
	for outcome in scenarios:
		# Para cada escenario, el posible numero de pickers.
		for n_pickers in range(5,max_number_pickers + 1):
			# Para cada posible escenario y numero de pickers realizamos las number_of_simulations simulaciones.
			times,revs = simulate_process(data, outcome, n_pickers, number_of_simulations)

			# Procesamos los resultados
			analyze_results(times,revs,n_pickers, outcome)

	### Posibilidad 2: realizar una ejecucion por cada combinacion cambiando las opciones de outcome y n_pickers.
	### Descomentar las siguientes lineas, e ir modificando el valor de las variables outcome y n_pickers.
	# outcome = outcomes[0]
	# n_pickers = 6
	## Para cada posible escenario y numero de pickers realizamos las number_of_simulations simulaciones.
	#times,revs = simulate_process(data, outcome, n_pickers, number_of_simulations)
	## Procesamos los resultados
	#analyze_results(times,revs)


if __name__ == '__main__':
	#print(simulate_uniform(20, 45))
	main()
